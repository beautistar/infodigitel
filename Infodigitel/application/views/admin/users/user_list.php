 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">  
<link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/upload_button.css">  
<script>
    $(document).ready(function(){
       $("#import").change(function(){
           $('#import-form').submit();
       })
    });
</script>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-body">
                <div class="col-md-6">
                    <h4><i class="fa fa-list"></i> &nbsp; WhatsApp</h4>
                </div>
                <div class="col-md-6 text-right">
                    <div class="btn-group margin-bottom-20">
                        <a href="<?= base_url('admin/users/export_csv'); ?>" class="btn btn-success" >Export CSV Template</a>
                        <div class="btn btn-success upload-btn-wrapper">
                             <?php  $attributes  = array("id"=>'import-form');
                                echo form_open_multipart('admin/users/import',$attributes); ?>
                                <button class="btn btnUpload">Import CSV</button>
                                <input type="file" id="import" name="userfile" />    
                        <?php echo form_close(); ?>
                        </div>
                    </div>                                   
                </div>

            </div>
        </div>
    </div>
    <div class="box border-top-solid">
        <!-- /.box-header -->
        <div class="box-body table-responsiv">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                      <!--<th><input type="checkbox" style="width: 20px; height: 20px;" id='checkall' ></th>-->
                        <th><input type="checkbox" style="width: 20px; height: 20px;" id='checkall' onchange="checkAll(this)" name="chk[]"></th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Mobile No.</th>
                        <th >Edit</th>
                        <th >Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($all_users as $row): ?>
                        <tr>
                          
                            <td ><input name="ids" type="checkbox" style="width: 20px; height: 20px;" class='checkbox' value=<?= $row['mobile_no']; ?>></td>
                            <td><?= $row['firstname']; ?></td>
                            <td><?= $row['lastname']; ?></td>
                            <td><?= $row['mobile_no']; ?></td>
                            <td class="text-left"><a href="<?= base_url('admin/users/edit/' . $row['id']); ?>" class="btn btn-info btn-flat btn-xs">Edit</a></td>
                            <td class="text-left"><a data-href="<?= base_url('admin/users/del/' . $row['id']); ?>" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#confirm-delete">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box border-top-solid">
            <!-- /.box-header -->
            <div class="box-body col-lg-12">

                <!-- form start -->
            <?php //echo form_open(base_url('admin/users/send_whatsappmessage/', 'id="submitForm"'));  ?>
                        <form action="users/send_whatsappmessage" id="submitForm" method="post">
                            <div class="input-group input-group-lg form-group">
                                <input type="hidden" name="phone_nos">
                                <textarea  name="message" style="width: 100%;" rows="3" class="form-control" placeholder="Type message here..." required></textarea>
                                <span class="input-group-btn">
                                    <Input type="button" name="btnsubmit" value="Send" id="btnsubmit" class="btn btn-info btn-flat" onclick="sendMessage('ids');">Send</Input>
                                </span>
                            </div>
                        </form>
            <?php // echo form_close( ); ?>

    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>  


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script> 
  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script>
  
<script>
$("#view_users").addClass('active');
</script> 

<script type="text/javascript">
 $(document).ready(function(){
 
  // Changing state of CheckAll checkbox 
  $(".checkbox").click(function(){
 
    if($(".checkbox").length == $(".checkbox:checked").length) {
      $("#checkall").prop("checked", true);
    } else {
      $("#checkall").removeAttr("checked");
    }
  });
});

 function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }
 
 function sendMessage(checkboxName) {
    var sendForm = document.getElementById('submitForm');
    
    var checkboxes = document.querySelectorAll('input[name="' + checkboxName + '"]:checked'), values = [];
    Array.prototype.forEach.call(checkboxes, function(el) {
        values.push(el.value);
    });
    if (values.length == 0) {
        return alert('Select at least 1 user');        
    }
    if (!sendForm.message.checkValidity()) {
        return alert('Enter message');
    }
    sendForm.phone_nos.value = values;
    //alert(sendForm.phone_nos);
    sendForm.submit();
}
</script>       
