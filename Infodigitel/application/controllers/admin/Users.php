<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Users extends MY_Controller {

		public function __construct(){
			parent::__construct();
            $this->load->library('excel');
			$this->load->model('admin/user_model', 'user_model');
		}

		public function index(){
			$data['all_users'] =  $this->user_model->get_all_users();
			$data['title'] = 'User List';
			$data['view'] = 'admin/users/user_list';
			$this->load->view('admin/layout', $data);
		}
		
		//---------------------------------------------------------------
		//  Add User
		public function add(){
			if($this->input->post('submit')){

				//$this->form_validation->set_rules('username', 'Username', 'trim|min_length[5]|required');
				$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required');
				$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
				//$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|is_unique[ci_users.email]|required');
				$this->form_validation->set_rules('mobile_no', 'Number', 'trim|required');
				//$this->form_validation->set_rules('password', 'Password', 'trim|required');
				//$this->form_validation->set_rules('group', 'Group', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['title'] = 'Add User';
					$data['user_groups'] = $this->user_model->get_user_groups();
					$data['view'] = 'admin/users/user_add';
					$this->load->view('admin/layout', $data);
				}
				else{
					$data = array(
						//'username' => $this->input->post('username'),
						'firstname' => $this->input->post('firstname'),
						'lastname' => $this->input->post('lastname'),
						//'email' => $this->input->post('email'),
						'mobile_no' => $this->input->post('mobile_no'),
						//'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
						//'role' => $this->input->post('group'),
						'created_at' => date('Y-m-d : h:m:s'),
						'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->user_model->add_user($data);
					if($result){
						$this->session->set_flashdata('msg', 'User has been Added Successfully!');
						redirect(base_url('admin/users'));
					}
				}
			}
			else{
				$data['user_groups'] = $this->user_model->get_user_groups();
				$data['title'] = 'Add User';
				$data['view'] = 'admin/users/user_add';
				$this->load->view('admin/layout', $data);
			}
			
		}

		//---------------------------------------------------------------
		//  Edit User
		public function edit($id = 0){
			if($this->input->post('submit')){
				//$this->form_validation->set_rules('username', 'Username', 'trim|required');
				$this->form_validation->set_rules('firstname', 'Username', 'trim|required');
				$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
				//$this->form_validation->set_rules('email', 'Email', 'trim|required');
				$this->form_validation->set_rules('mobile_no', 'Number', 'trim|required');
				//$this->form_validation->set_rules('status', 'Status', 'trim|required');
				//$this->form_validation->set_rules('group', 'Group', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['user'] = $this->user_model->get_user_by_id($id);
					$data['user_groups'] = $this->user_model->get_user_groups();
					$data['title'] = 'Edit User';
					$data['view'] = 'admin/users/user_edit';
					$this->load->view('admin/layout', $data);
				}
				else{
					$data = array(
						//'username' => $this->input->post('username'),
						'firstname' => $this->input->post('firstname'),
						'lastname' => $this->input->post('lastname'),
						//'email' => $this->input->post('email'),
						'mobile_no' => $this->input->post('mobile_no'),
						//'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
						//'role' => $this->input->post('group'),
						//'is_active' => $this->input->post('status'),
						'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->user_model->edit_user($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'User has been Updated Successfully!');
						redirect(base_url('admin/users'));
					}
				}
			}
			else{
				$data['user'] = $this->user_model->get_user_by_id($id);
				$data['user_groups'] = $this->user_model->get_user_groups();
				$data['title'] = 'Edit User';
				$data['view'] = 'admin/users/user_edit';
				$this->load->view('admin/layout', $data);
			}
		}

		//---------------------------------------------------------------
		//  Delete Users
		public function del($id = 0){
			$this->db->delete('ci_users', array('id' => $id));
			$this->session->set_flashdata('msg', 'User has been Deleted Successfully!');
			redirect(base_url('admin/users'));
		}

		//---------------------------------------------------------------
		//  Export Users PDF 
		public function create_users_pdf(){
			$this->load->helper('pdf_helper'); // loaded pdf helper
			$data['all_users'] = $this->user_model->get_all_users();
			$this->load->view('admin/users/users_pdf', $data);
		}

		//---------------------------------------------------------------	
		// Export data in CSV format 
		public function export_csv(){ 
		   // file name 
		   $filename = 'users_phone_list_'.date('Y-m-d').'.csv'; 
		   header("Content-Description: File Transfer"); 
		   header("Content-Disposition: attachment; filename=$filename"); 
		   header("Content-Type: application/csv; ");
		   
		   // get data 
		   $user_data = $this->user_model->get_all_users_for_csv();

		   // file creation 
		   $file = fopen('php://output', 'w');
		 
		   $header = array("First Name", "Last Name", "whatsapp Number"); 
		   fputcsv($file, $header);
		   foreach ($user_data as $key=>$line){ 
		     fputcsv($file,$line); 
		   }
		   fclose($file); 
		   exit; 
		}
        
        public function import() {

            $file_info = pathinfo($_FILES["userfile"]["name"]);
            $file_directory = "uploads/";
            $new_file_name = date("d-m-Y ") . rand(000000, 999999) . "." . $file_info["extension"];

            if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $file_directory . $new_file_name)) {
                $file_type = PHPExcel_IOFactory::identify($file_directory . $new_file_name);
                $objReader = PHPExcel_IOFactory::createReader($file_type);
                $objPHPExcel = $objReader->load($file_directory . $new_file_name);
                $sheet_data = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                $count = count($sheet_data);
                
                for ($i = 2; $i <= $count; $i++) {
                    $dataArray = array(
                        'firstname' => $sheet_data[$i]['A'],
                        'lastname' => $sheet_data[$i]['B'],
                        'mobile_no' => $sheet_data[$i]['C'],
                    );
                    $this->user_model->add_user($dataArray);
                }
            }
            redirect("admin/users");
        }
          
        public function send_whatsappmessage(){                             
                
            $this->form_validation->set_rules('message', 'Message', 'trim|required');
            
            if ($this->form_validation->run() == FALSE) {
                
                $data['title'] = 'Error'; 
                $data['all_users'] = $this->user_model->get_all_users();                   
                $data['view'] = 'admin/users/user_list';
                $this->load->view('admin/layout', $data);
            }
            else{                
                $literal_phone_nos =  $this->input->post('phone_nos');
                $phone_nos = explode(',', $literal_phone_nos) ;
                $message = $this->input->post('message');
                
                foreach ($phone_nos as $phone) {
                
                    $data = [                        
                        'phone' => $phone, // Receivers phone
                        'body' => $message, //$message, // Message
                    ];
                    $json = json_encode($data); // Encode data to JSON
                    // URL for request POST /message
                    $url = 'https://eu1.whatsapp.chat-api.com/instance1004/message?token=40pyjonrra0h9di1';
                    // Make a POST request
                    $options = stream_context_create(['http' => [
                            'method'  => 'POST',
                            'header'  => 'Content-type: application/json',
                            'content' => $json
                        ]
                    ]);
                    // Send a request
                    $result = file_get_contents($url, false, $options);                    
                    if($result){

                    } else {
                      
                    }                    
                }
                
                $this->session->set_flashdata('msg', 'Whatsapp message has been sent Successfully!');
                redirect(base_url('admin/users'));
                
            }
        }
        
        public function send_whatsappmessage_curl(){                             
                
            $this->form_validation->set_rules('message', 'Message', 'trim|required');
            
            if ($this->form_validation->run() == FALSE) {
                
                $data['title'] = 'Error'; 
                $data['all_users'] = $this->user_model->get_all_users();                   
                $data['view'] = 'admin/users/user_list';
                $this->load->view('admin/layout', $data);
            }
            else{                
                //$phone_nos[] = explode(',', $this->input->post('phone_nos')) ;
                $phone_nos[] =  $this->input->post('phone_nos');
                $message = $this->input->post('message');
                //print_r($phone_nos);
                
                foreach ($phone_nos as $phone) {
                    
                    $data = [
                        'phone' => $phone, // Receivers phone
                        'body' => $message, // Message
                    ];
                    $json = json_encode($data); // Encode data to JSON
                    // URL for request POST /message
                    $url = 'https://eu1.whatsapp.chat-api.com/instance877/message?token=zo0it870p6tnqxsm';
                    
                    $headers = array(
                        'Method: POST',
                        'Content-Type: application/json'
                    );
        
                    // Send a request
                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 1);

                    //@curl_exec($ch);
                    
                    $result = curl_exec($ch); 
                    curl_close($ch);
                    print_r($result);                    
                }
                
            }
        }
	}


?>